<?php

namespace App\Controller\Main;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LandingController extends AbstractController
{
    /**
     * @return Response
     * @Route("/", name="locals")
     */
    public function main(): Response
    {
        return $this->render('base.html.twig');
    }
}